FROM alpine:edge AS scripts

ARG http_proxy
ARG author
ARG source_uri_scripts

ENV http_proxy=$http_proxy
ENV https_proxy=$http_proxy

LABEL author="github.com/$author"
LABEL description="custom dotfiles container for concourse build"

RUN echo -e "http_proxy=$http_proxy\nhttps_proxy=$https_proxy" >> /etc/environment \
  && echo "export PATH=$PATH:$HOME/.local/bin" > /etc/profile.d/scripts \
  && apk update \
  && apk add bash python3 stow git \
  && git clone --branch release "$source_uri_scripts" scripts \
  && rm -rf /var/cache/*

FROM scripts
RUN bash /scripts/install.sh
