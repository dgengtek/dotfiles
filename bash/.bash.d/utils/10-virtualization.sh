#!/usr/bin/env bash

f_virsh_get_vm() {
  virsh list --all --name | fzf --select-1 --exit-0
}

f_virsh_get_vm_multi() {
  virsh list --all --name | fzf --multi --select-1 --exit-0
}

f_virsh_start() {
  local -r selected=($(f_virsh_get_vm_multi))
  [[ ${#selected[@]} -eq 0 ]] && echo "No domain selected." >&2 && return 1
  for vm in "${selected[@]}"; do
    virsh start "$vm"
  done
}

f_virsh_get_vmsnapshot() {
  local -r domain=${1:?"No domain given."}
  local -r snapshot=$(virsh snapshot-list "$domain" --name | fzf --select-1 --exit-0)
  [[ -z $snapshot ]] && return 1
  echo "$snapshot"
}

f_virsh_get_vmsnapshot_select() {
  local -r selected=($(f_virsh_get_vm_multi))
  [[ ${#selected[@]} -eq 0 ]] && echo "No domain selected." >&2 && return 1
  local snapshot=
  for vm in "${selected[@]}"; do
    snapshot=$(virsh snapshot-list "$vm" --name | fzf --select-1 --exit-0)
    [[ -z $snapshot ]] && continue
    echo -n "$vm $snapshot"
  done
}

virsh_snapshot_create_default() {
  local -r domain=${1:?"==> ERROR: No domain given."}
  local -r name="snapshot-$(id -un)-$(date +%Y%m%d%H%M%S)"
  local -r description="$@"
  virsh snapshot-create-as "$domain" --name "$name" --description "$description"
}

virsh_snapshot_revert_current() {
  local -r domain=$(f_virsh_get_vm)
  if [[ -z "$domain" ]]; then
    echo "No domain selected." >&2
    return 1
  fi
  virsh snapshot-revert "$domain" --current
}

virsh_snapshot_revert() {
  local -r domain=$(f_virsh_get_vm)
  if [[ -z "$domain" ]]; then
    echo "No domain selected." >&2
    return 1
  fi
  local -r snapshotname=$(f_virsh_get_vmsnapshot "$domain")
  [[ -z "$snapshotname" ]] && return 1
  virsh snapshot-revert "$domain" --snapshotname "$snapshotname"
}

virsh_delete_snapshot_select() {
  local -r domain=${1:?"==> ERROR: No domain given."}
  snapshot=(f_virsh_get_vmsnapshot)
  (($? != 0)) && return 1
  virsh snapshot-delete "$domain" "$snapshot" || return 1
}

virsh_delete_snapshots() {
  local -r domain=${1:?"==> ERROR: No domain given."}
  snapshots=($(virsh snapshot-list --name "$domain"))
  (($? != 0)) && return 1
  for snapshot in "${snapshots[@]}"; do
    virsh snapshot-delete "$domain" "$snapshot" || return 1
  done
}

virsh_delete() {
  local -r domain=${1:?"==> ERROR: No domain given."}
  read -p "Are you sure you want to delete the VM '$domain' and all its snapshots? >" answer
  case $answer in
    y|Y|yes|YES)
      ;;
    *)
      return 1
      ;;
  esac
  if ! virsh_delete_snapshots "$domain"; then
    return 1
  fi
  virsh undefine "$domain" --remove-all-storage
}

virsh_delete_force() {
  local -r domain=${1:?"==> ERROR: No domain given."}
  virsh destroy "$domain" && virsh_delete "$domain"
}
