#!/usr/bin/env bash
xrandr_hd() {
    xrandr --output DVI-0 --mode 1920x1080 --rate 60 --left-of DVI-1 --primary
    xrandr --output DVI-1 --mode 1920x1080 --rate 60 --pos 1920x0
}
xrandr_game() {
    xrandr --output DVI-0 --mode 1600x900  --left-of DVI-1 --primary
    xrandr --output DVI-1 --auto
}
xrandr_hz() {
    xrandr --output DVI-0 --mode 1280x1024 --rate 75 --left-of DVI-1 --primary
    xrandr --output DVI-1 --mode 1152x864 --rate 75 --pos 1280x0
}
