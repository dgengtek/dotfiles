" use vi improved
set nocompatible              
filetype off                  
filetype plugin indent on
" coloring
if (&term == "screen" || &term == "screen-bce" || &term == "xterm" || &term == "xterm-256color")
  set t_Co=256
endif

if has("syntax")
  syntax on                      
endif

let g:ycm_server_python_interpreter = '/usr/bin/python3'

" sudo overwrite file
cmap w!! w !sudo tee > /dev/null %

set backupskip+=test,tmp,temp

set encoding=utf-8
set background=dark

" concealing
set conceallevel=2
set concealcursor=c

" use :set list to enable
" use :set nolist to disable
" list chars tab,eol,trail etc
" default eol
" set listchars+=eol:$
" tab char
set listchars+=tab:>-
" trailing space char
set listchars+=trail:-
set listchars+=precedes:<,extends:>

set nojoinspaces

set wrap
set sidescroll=20
set textwidth=0
set wrapmargin=0
set splitbelow
set splitright


set linebreak
set scrolloff=999
set sidescrolloff=999

" expand tab to spaces with '>' and '<'; and if autoindent is on
" insert real tab with ctrl-v<Tab>
set expandtab

" default tabstop
" set tabstop=8
" make tabs appear to be 2 spaces long
set softtabstop=2
" shifts used for '<' '>'
set shiftwidth=2
" set noexpandtab


set showmatch  " Show matching brackets.
set matchtime=2  " Bracket blinking.
"set novisualbell  " No blinking
"set noerrorbells  " No noise.
set laststatus=2  " Always show status line.
set ruler  " Show ruler

" disable mouse
set mouse=
" hide mouse when typing
"set mousehide  

" ignore case when all chars are lower
set ignorecase
set smartcase

" menu completion with tab in cmd mode
set wildmenu                  
" matches all
set wildmode=full             
" Ignore these files when completing
set wildignore+=*.o,*.obj,.git,*.pyc
set wildignore+=eggs/**
set wildignore+=*.egg-info/**

" highlight search
set hlsearch
"highlight code longer than 80
highlight OverLength ctermbg=red ctermfg=white guibg=#660000
match OverLength /\%81v.\+/


set pastetoggle=<F10>

set number
set relativenumber
if has("unnamedplus")
  " yank into + register, * all 
  set clipboard=unnamedplus
else
  " all op into * register
  set clipboard=unnamed
endif


"if has("autocmd")
  filetype plugin indent on
  autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o 
  autocmd FileType html setlocal shiftwidth=2 softtabstop=2 expandtab
  autocmd FileType python setlocal shiftwidth=4 softtabstop=4
  autocmd FileType bash setlocal shiftwidth=2 softtabstop=2
  autocmd FileType java setlocal shiftwidth=4 softtabstop=4
  autocmd FileType make set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab
"endif

" asciidoc
" mkdir ~/.vim/syntax
" wget https://raw.githubusercontent.com/asciidoc/asciidoc/master/vim/syntax/asciidoc.vim -O .vim/syntax/asciidoc.vim
" :set syn=asciidoc
au BufRead,BufNewFile *.adoc setfiletype asciidoc
au BufRead,BufNewFile *.asciidoc setfiletype asciidoc
au BufRead,BufNewFile *.sls setfiletype sls
au BufRead,BufNewFile *.yaml,*.yml setfiletype yaml
au BufRead,BufNewFile *.toml setfiletype toml
au BufRead,BufNewFile *.rs setfiletype rust


set statusline=%f "tail of the filename
set statusline+=%#warningmsg#
set statusline+=%*

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

if !empty(glob("~/.vim/bundle/Vundle.vim"))
  " install in bundle path
  call vundle#begin("~/.vim/bundle")
  Plugin 'gmarik/Vundle.vim'
  Plugin 'vimwiki/vimwiki'
  Plugin 'junegunn/fzf'
  Plugin 'SirVer/ultisnips'
  Plugin 'honza/vim-snippets'
  Plugin 'scrooloose/nerdcommenter'
  Plugin 'scrooloose/nerdtree'
  Plugin 'tpope/vim-surround'
  Plugin 'saltstack/salt-vim'
  Plugin 'Chiel92/vim-autoformat'
  Plugin 'https://github.com/fatih/vim-go'
  Plugin 'Glench/Vim-Jinja2-Syntax'
  Plugin 'Valloric/YouCompleteMe'
  Plugin 'stephpy/vim-yaml'
  Plugin 'scrooloose/syntastic'
  Plugin 'lervag/vimtex'
  Plugin 'https://github.com/elzr/vim-json'
  Plugin 'https://github.com/rust-lang/rust.vim'
  Plugin 'https://github.com/cespare/vim-toml'

  call vundle#end()            
  " " help vundle
  " " :PluginList       - lists configured plugins
  " " :PluginInstall    - installs plugins; append `!` to update or just
  " :PluginUpdate
  " " :PluginSearch foo - searches for foo; append `!` to refresh local cache
  " " :PluginClean      - confirms removal of unused plugins; append `!` to
  " auto-approve removal
  " "
  " " see :h vundle for more details or wiki for FAQ
endif

" reload vim on changes with vimrc
augroup myvimrc
  au!
  au BufWritePost .vimrc,_vimrc,vimrc,.gvimrc,_gvimrc,gvimrc so $MYVIMRC | if has('gui_running') | so $MYGVIMRC | endif
augroup END

let g:UltiSnipsExpandTrigger="<c-b>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" rustfmt
let g:autofmt_autosave = 1


" mappings
noremap <F3> :Autoformat<CR><CR>
nnoremap <F5> :!%:p<CR>
" select from buffers
nnoremap <F9> :buffers<CR>:buffer<Space>
" stop cr
vnoremap // y/<C-R>"<CR>

nmap ,cs :let @*=expand("%")<CR>
nmap ,cl :let @*=expand("%:p")<CR>


if !empty(glob("~/.vim/bundle/nerdtree"))
  nmap :E :NERDTreeToggle<CR>
endif

let g:ftplugin_sql_omni_key = '<Leader>sql'


set modeline
" Append modeline after last line in buffer.
" Use substitute() instead of printf() to handle '%%s' modeline in LaTeX
" files.
function! AppendModeline()
  let l:modeline = printf(" vim: set ts=%d sw=%d tw=%d %set :",
        \ &tabstop, &shiftwidth, &textwidth, &expandtab ? '' : 'no')
  let l:modeline = substitute(&commentstring, "%s", l:modeline, "")
  call append(line("$"), l:modeline)
endfunction
nnoremap <silent> <Leader>ml :call AppendModeline()<CR>


" vimwiki paths
let g:vimwiki_hl_headers = 1
let g:vimwiki_hl_cb_checked = 1
let g:vimwiki_url_maxsave = 40
let g:vimwiki_conceallevel = 2


let wiki_1 = {}
let wiki_1.path = '~/vimwiki/std/'
let wiki_1.path_html = '~/vimwiki/std/html'
let wiki_1.auto_tags= 1

let g:vimwiki_list = [wiki_1]
